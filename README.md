
# py-irc #

Python modules to manage IRC protocol.

Copyright (C) 2012 Rafa Couto

Authors: 

* Rafa Couto <rafa@aplicacionesyredes.com>


## LICENSE ##

This program is free software: you can redistribute it and/or modify
it under the terms of the **GNU General Public License** as published 
by the Free Software Foundation, either **version 3** of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See LICENCE.txt for a full text version.


## DEVELOPERS ##

You are invited to use, fork, improve and pull over GIT repository at

https://bitbucket.org/rafacouto/py-irc

Please, do not miss to mention this library if you use it :)


## ROADMAP ##

1.0 - IRC basics

- IrcClient class ready to connect against an IRC server, join a channel,
send and receive messages.

Futures

- An abstract class to implement an IRC bot with plugins.
- Bot plugin to log channel conversations.
- Bot plugin to manage a channel in freenode http://freenode.net/
- Bot plugin to listen to users and to answer FAQs.
- Bot plugin to send messages through IM networks.


