#!/usr/bin/env python

""" core.py: Common classes in this module. """

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__      = "Rafa Couto"
__copyright__   = "Copyright 2012, aplicacionesYredes.com"
__credits__     = ["Rafa Couto"]
__license__     = "GPLv3"
__version__     = "0.1"
__maintainer__  = "Rafa Couto"
__email__       = "rafa@aplicacionesyredes.com"



import re


class EventHook(object):

	def __init__(self):
		self._handlers = []


	def __iadd__(self, handler):
		self._handlers.append(handler)


	def __isub__(self, handler):
		self._handlers.remove(handler)


	def fire(self, *args, **keywargs):
		for handler in self._handlers:
			handler(*args, **keywargs)



REGEX_MESSAGE = r"^(:([^ ]+) )?(([a-zA-Z]+)|([0-9]{1,3}))( (.+))?$"

class IrcMessage:
	
	def __init__(self, sender, command, params):
	
		self._sender = sender
		self._command = command
		self._params = params

	
	def __str__(self):

		if self._sender != None:
			result = ":%s %s" % (self._sender, self._command)
		else:
			result = self._command

		for param in self._params:
			if param:
				result += " " + param

		return result


	@staticmethod
	def fromStr(message):
		m = re.match(REGEX_MESSAGE, message)
		if m:
			sender = m.group(2)
			command = m.group(3)
			rawparams = m.group(7)			
			if ' :' in rawparams:
				(first, second) = rawparams.split(' :', 2)
				params = first.split(' ')
				params.append(second)
			else:
				params = rawparams.split(' ')				
			return IrcMessage(sender, command, params)
		else:
			raise SyntaxError, "Error parsing IRC message."


	@property
	def sender(self):
		return self._sender

	@property
	def command(self):
		return self._command

	@property
	def params(self):
		return self._params
		
	
